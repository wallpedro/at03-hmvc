## Módulo de Recibos

Esse módulo é responsável por administrar os recibos.

1. Gera recibos a partir de um formulário genérico.
2. Persiste os dados do formulário no banco 'recibo_log'.
3. Lista os recibos gerados e persistidos no banco de dados.
4. Disponibiliza para download todos os recibos gerados e armazenados no banco de dados.
5. Formato de saída: PDF.

---

## Dependências

Dependências (externas) para a execução do projeto:

1. JQuery.
2. JQuery Mask.
3. JQuery Validation.
4. Jquery DatePicker.
5. mPDF.

Todas elas estão inclusas no sistemas de arquivos deste repositório e já estão sendo carregadas.

---

## Banco de dados

Todos os recibos gerados são persistidos na tabela recibo_log.

1. No diretório sql, o arquivo recibo.sql tem o script para gerar a tabela de persistência de recibos.
2. Caso a tabela esteja vazia, o **teste de consulta de dados do recibo** acusará um falso negativo.

---

## Campos de preenchimento obrigatório

Todos os campos do formulário usado para gerar um novo recibo estão definidos como obrigatório. Para editar:

1. Vá até assets/recibo.
2. No arquivo recibo.js, localize, na linha 17, o método validate.
3. O parâmetro dessa função é um objeto que define as regras das entradas do formulário. Para cada campo do formulário (identificado pelo atributo 'name'), há a regra 'required: true' definida. 
4. Para remover a obrigatoriedade ou acrescentar novas regras (conforme especificado em https://jqueryvalidation.org/documentation/), remover ou editar tais regras.

---