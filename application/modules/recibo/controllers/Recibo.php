<?php

require_once APPPATH . 'modules\recibo\libraries\vendor\autoload.php';

use \Mpdf\Mpdf;

class Recibo extends MY_Controller {

    function __construct() {
        $this->load->model('ReciboModel', 'model');
    }

    public function index() {
        $this->add_style('recibo/jquery-datepicker');
        $this->add_script('recibo/jquery-3.4.1');
        $this->add_script('recibo/jquery-1.12.4');
        $this->add_script('recibo/jquery-ui-1.12.4');
        $this->add_script('recibo/jquery-validation');
        $this->add_script('recibo/jquery-validation.min');
        $this->add_script('recibo/jquery-validation-aditional-methods');
        $this->add_script('recibo/jquery-validation-aditional-methods.min');
        $this->add_script('recibo/jquery.mask');
        $this->add_script('recibo/recibo');
        $form['form'] = $this->load->view('form', '', true);
        $html = '';
        $html .= $this->load->view('main_layout', $form, true);
        $html .= $this->model->getLogTableHTML();
        $this->show($html);
    }

    public function gerarPdf($dadosRecibo) {
        $html = $this->load->view('reciboTemplate', $dadosRecibo, true);
        $mpdf = new Mpdf();
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    public function gerarPdfFromForm() {
        $dadosRecibo = $this->model->dadosReciboFromPostToArray();
        $this->model->listener($dadosRecibo);
        $this->gerarPdf($dadosRecibo);
    }

    public function gerarPdfFromLog() {
        $id = $this->input->post('idRecibo');
        $recibo = $this->model->getLog(['idRecibo' => $id], 1);
        $this->gerarPdf(array_shift($recibo));
    }
}