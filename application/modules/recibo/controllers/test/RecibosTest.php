<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/recibo/libraries/Recibos.php';
include_once APPPATH . 'modules/recibo/libraries/MySqlPersistance.php';

class RecibosTest extends Toast {

    private $recibos;
    private $persistance;

    function __construct(){
        parent::__construct('RecibosTest');
    }

    function _pre(){
        $this->recibos = new Recibos();
        $this->persistance = new MySqlPersistance();
    }

    function test_observer_list_inicializa_vazio(){
        $observerList = $this->recibos->getObserverList();
        $this->_assert_empty($observerList, 'Espera-se que o array inicialize vazio');
    }

    function test_add_observer(){
        $qtdObserverInicial = count($this->recibos->getObserverList());
        $this->recibos->addObserver($this->persistance);
        $qtdObserverFinal = count($this->recibos->getObserverList());
        $qtdEsperado = $qtdObserverInicial + 1;
        $this->_assert_equals($qtdObserverFinal, $qtdEsperado, 'Esperado: '.$qtdEsperado.'. Obtido: '.$qtdObserverFinal);
    }

    function test_remove_observer(){
        $this->recibos->addObserver($this->persistance);
        $qtdObserverInicial = count($this->recibos->getObserverList());
        $this->recibos->removeObserver($this->persistance);
        $qtdObserverFinal = count($this->recibos->getObserverList());
        $qtdEsperado = $qtdObserverInicial - 1;
        $this->_assert_equals($qtdObserverFinal, $qtdEsperado, 'Esperado: '.$qtdEsperado.'. Obtido: '.$qtdObserverFinal);
    }

    function test_get_log(){
        $logs = $this->recibos->getDados($this->persistance);
        $this->_assert_not_empty($logs, 'Esperado: Array com logs. Obtido: Array vazio');
    }
}