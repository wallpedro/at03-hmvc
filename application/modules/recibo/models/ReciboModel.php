<?php

include_once APPPATH.'modules\recibo\libraries\MySqlPersistance.php';
include_once APPPATH.'modules\recibo\libraries\Recibos.php';

class ReciboModel extends CI_Model {

    function __construct() {
        $this->load->library('Recibos', 'recibo');
    }

    public function listener($dadosRecibo){
        //$this->load->dbutil();
        $mysql = new MySqlPersistance();
        $reciboObj = new Recibos();
        $reciboObj->addObserver($mysql);
        $reciboObj->novoRecibo($dadosRecibo);
    }

    public function dadosReciboFromPostToArray(){
        $dadosRecibo['numeroRecibo'] = $this->input->post('numeroRecibo');
        $dadosRecibo['dataRecibo'] = $this->input->post('dataRecibo');
        $dadosRecibo['nomePagador'] = $this->input->post('nomePagador');
        $dadosRecibo['enderecoPagador'] = $this->input->post('enderecoPagador');
        $dadosRecibo['valorRecibo'] = $this->input->post('valorRecibo');
        $dadosRecibo['assuntoRecibo'] = $this->input->post('assuntoRecibo');
        $dadosRecibo['numeroCheque'] = $this->input->post('numeroCheque');
        $dadosRecibo['bancoCheque'] = $this->input->post('bancoCheque');
        $dadosRecibo['agenciaCheque'] = $this->input->post('agenciaCheque');
        $dadosRecibo['nomeEmitente'] = $this->input->post('nomeEmitente');
        $dadosRecibo['docEmitente'] = $this->input->post('docEmitente');
        $dadosRecibo['enderecoEmitente'] = $this->input->post('enderecoEmitente');

        return $dadosRecibo;
    }

    public function getLog($where = null, $limit = null){
        $mysql = new MySqlPersistance();
        $reciboObj = new Recibos();
        return $reciboObj->getDados($mysql, $where, $limit);
    }

    public function getLogTableHTML(){
        $logRecibos = $this->getLog();
        $rows['logRows'] = '';
        foreach ($logRecibos as $logRecibo){
            $rows['logRows'] .= $this->load->view('logRow', $logRecibo, true);
        }
        return $this->load->view('recibos_modal', $rows, true);
    }

}