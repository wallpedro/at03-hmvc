<?php


class Recibos {

    private $observerList = [];

    public function addObserver(PersistanceObject $observer) {
        $this->observerList[] = $observer;
    }

    public function removeObserver(PersistanceObject $observer) {
        foreach ($this->observerList as $key => $value) {
            if ($observer == $value) {
                unset($this->observerList[$key]);
            }
        }
    }

    private function persist($dadosRecibo) {
        foreach ($this->observerList as $observer) {
            $observer->persist($dadosRecibo);
        }
    }

    public function novoRecibo($dadosRecibo) {
        $this->persist($dadosRecibo);
    }

    public function getDados(PersistanceObject $persistanceObject, Array $where = null, $limit = null) {
        return $persistanceObject->getData($where, $limit);
    }

    public function getObserverList(){
        return $this->observerList;
    }

}