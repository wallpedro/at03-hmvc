<?php


interface PersistanceObject {

    public function persist(Array $dadosRecibo);

    public function getData(Array $where = null, $limit = null);
}