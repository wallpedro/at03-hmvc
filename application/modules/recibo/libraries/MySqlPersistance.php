<?php

require_once 'PersistanceObject.php';

class MySqlPersistance extends CI_Object implements PersistanceObject {

    public function persist(Array $dadosRecibo) {
        foreach ($dadosRecibo as $key => $value) {
            $data[$key] = $value;
        }
        $this->db->insert('recibo_log', $data);
    }

    public function getData(Array $where = null, $limit = null) {
        $qry = $this->db->get_where('recibo_log', $where, $limit);
        return $qry->result_array();
    }

    public function dataReciboParaDB(String $data) {
        $dia = substr($data, 0, 2);
        $mes = substr($data, 3, 2);
        $ano = substr($data, 6, 6);
        return $ano . '-' . $mes . '-' . $dia;
    }
}