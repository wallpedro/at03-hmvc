<div class="card">
    <div class="card-header text-center info-color white-text">
        <h2>Novo Recibo</h2>
    </div>
    <div class="card-body">
        <form class="text-center" id="reciboForm" style="color: #757575;" method="post" autocomplete="off" action="<?= base_url('/recibo/gerarPdfFromForm') ?>">
            <input autocomplete="false" name="hidden" type="text" style="display:none;">
            <div class="row">
                <div class="col-sm-4">
                    Número do recibo<input type="text" id="numeroRecibo" class="form-control" name="numeroRecibo">

                </div>
                <div class="col-sm-4">
                    Data do recibo<input type="text" id="dataRecibo" class="form-control datepicker" name="dataRecibo">
                </div>
            </div>
            <div class="card mt-5">
                <div class="card-header">
                    <h4>Dados do pagador</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            Nome<input type="text" id="nomePagador" class="form-control" name="nomePagador">
                        </div>
                        <div class="col-sm-6">
                            Endereço<input type="text" id="enderecoPagador" class="form-control" name="enderecoPagador">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header">
                    <h4>Dados do pagamento</h4>
                </div>
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-sm-6">
                            Valor do recibo<input type="text" id="valorRecibo" class="form-control valorRecibo" name="valorRecibo">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12">
                            Motivo<input type="text" id="assuntoRecibo" class="form-control" name="assuntoRecibo">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-3">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="reciboCheque" value="1"
                                       onchange="Recibo.cheque()">
                                <label class="custom-control-label" for="reciboCheque">Pagamento com cheque?</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3" id="cheque" style="display: none">
                        <div class="col-sm-4">
                            Número do cheque<input type="text" id="numeroCheque" class="form-control" name="numeroCheque">
                        </div>
                        <div class="col-sm-4">
                            Banco<input type="text" id="bancoCheque" class="form-control" name="bancoCheque">
                        </div>
                        <div class="col-sm-4">
                            Agência<input type="text" id="agenciaCheque" class="form-control" name="agenciaCheque">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header">
                    <h4>Dados do emitente</h4>
                </div>
                <div class="card-body">
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            Nome<input type="text" id="nomeEmitente" class="form-control" name="nomeEmitente">
                        </div>
                        <div class="col-sm-6">
                            CPF/CNPJ/RG<input type="text" id="docEmitente" class="form-control" name="docEmitente">
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-12">
                            Endereço<input type="text" id="enderecoEmitente" class="form-control" name="enderecoEmitente">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <input type="submit" id="geraReciboSubmit" class="btn btn-info" value="Gerar recibo">
                <button type="button" id="listarLog" class="btn btn-info" data-toggle="modal" data-target="#basicExampleModal">
                    Listar recibos gerados
                </button>
            </div>
        </form>
    </div>
</div>