<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="modalRecibos"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalRecibos">Lista de Recibos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="text-center" id="reciboForm" style="color: #757575;" method="post"
                  action="<?= base_url('/recibo/gerarPdfFromLog') ?>">
                <div class="modal-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Pagador</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Emitente</th>
                            <th scope="col">Download PDF</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?= $logRows ?>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Download">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>