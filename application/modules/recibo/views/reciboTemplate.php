<div class="container" style="text-align: center">
    <h1 style="font-weight: bold;">RECIBO</h1>
</div>
<div class="container" style="text-align: left">
    <p><span style="font-weight: bold">RECIBO Nº: </span> <?= $numeroRecibo ?></p>
    <p><span style="font-weight: bold">DATA: </span> <?= $dataRecibo ?></p>
    <hr>
    <h3 style="text-align: center">DADOS DO PAGADOR</h3>
    <p><span style="font-weight: bold">NOME: </span> <?= $nomePagador ?></p>
    <p><span style="font-weight: bold">ENDEREÇO: </span> <?= $enderecoPagador ?></p>
    <hr>
    <p><span style="font-weight: bold">VALOR: </span> <?= $valorRecibo ?></p>
    <p><span style="font-weight: bold">REFERENTE A: </span> <?= $assuntoRecibo ?></p>
    <hr>
    <div class="container" style="text-align: left">
        <h3 style="text-align: center">PAGAMENTO EM CHEQUE</h3>
        <p><span style="font-weight: bold">CHEQUE Nº: </span> <?= $numeroCheque ?></p>
        <p><span style="font-weight: bold">BANCO: </span> <?= $bancoCheque ?></p>
        <p><span style="font-weight: bold">AGÊNCIA: </span> <?= $agenciaCheque ?></p>
    </div>
    <hr>
    <h3 style="text-align: center">DADOS DO EMITENTE</h3>
    <p><span style="font-weight: bold">NOME: </span> <?= $nomeEmitente ?></p>
    <p><span style="font-weight: bold">CPF/CNPJ/RG: </span> <?= $docEmitente ?></p>
    <p><span style="font-weight: bold">ENDEREÇO: </span> <?= $enderecoEmitente ?></p>
</div>