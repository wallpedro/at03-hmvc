create table recibo_log
(
    idRecibo         int auto_increment
        primary key,
    numeroRecibo     int                                 null,
    dataRecibo       date                                null,
    nomePagador      varchar(255)                        null,
    enderecoPagador  varchar(255)                        null,
    valorRecibo      float                               null,
    assuntoRecibo    varchar(255)                        null,
    numeroCheque     varchar(50)                         null,
    bancoCheque      varchar(100)                        null,
    agenciaCheque    varchar(50)                         null,
    nomeEmitente     varchar(255)                        null,
    docEmitente      varchar(50)                         null,
    enderecoEmitente varchar(255)                        null,
    timestamp        timestamp default CURRENT_TIMESTAMP null
);