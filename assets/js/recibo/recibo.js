class Recibo {
    static cheque() {
        if (document.getElementById('reciboCheque').checked) {
            $('#cheque').css('display', 'flex');
        } else {
            $('#cheque').css('display', 'none');
        }
    }
}

$('document').ready(function () {
    $('#valorRecibo').mask("000.000.000.000.000,00", {reverse: true});
    $('#numeroRecibo').mask("#");
    $('#dataRecibo').mask('00/00/0000');
    $('.datepicker').datepicker();
    $('.datepicker').datepicker('option', 'dateFormat', 'dd/mm/yy');
    $('#reciboForm').validate({
        rules: {
            numeroRecibo: {
                required: true,
                integer: true
            },
            dataRecibo: {
                required: true
            },
            nomePagador: {
                required: true
            },
            enderecoPagador: {
                required: true
            },
            valorRecibo: {
                required: true
            },
            assuntoRecibo: {
                required: true
            },
            nomeEmitente: {
                required: true
            },
            docEmitente: {
                required: true
            },
            enderecoEmitente: {
                required: true
            }
        },
        messages: {
            numeroRecibo: {
                required: 'Insira um número de recibo',
                integer: 'Apenas números são permitidos'
            },
            dataRecibo: {
                required: 'Insira uma data para o recibo'
            },
            nomePagador: {
                required: 'Informe o nome'
            },
            enderecoPagador: {
                required: 'Informe o endereço'
            },
            valorRecibo: {
                required: 'Informe um valor para o recibo'
            },
            assuntoRecibo: {
                required: 'Informe o motivo do pagamento'
            },
            nomeEmitente: {
                required: 'Informe o nome do emitente'
            },
            docEmitente: {
                required: 'Informe o documento do emitente'
            },
            enderecoEmitente: {
                required: 'Informe o endereço do emitente'
            }
        }
    });
});
